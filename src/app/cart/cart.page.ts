import { Component } from "@angular/core";
import { AlertController, NavController, PopoverController, ModalController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { TouchSequence } from 'selenium-webdriver';

import { ShopifyService } from "../services/shopify.service";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.page.html",
  styleUrls: ["./cart.page.scss"]
})
export class CartPage {
  public checkoutId: any = "";

  public cartItems: any = []; // array of items in checkout
  public itemIds: any = [];  // array of the item ids - efficient for removing all items

  public totalPrice: string; // total price of cart
  public currencyCode: any; // type of currency being used
  public orderStatus: any;
  public webUrl;

  public myBoolean: boolean = false;
  public isChecked : boolean;

  constructor(
    private navCtrl: NavController,
    private shopifyService: ShopifyService,
    private alertController: AlertController,
    private storage: Storage,
    private iab: InAppBrowser
  ) { }

  // Update cart everytime the page is opened
  ionViewWillEnter() {
    this.storage.get("checkoutId").then(id => {
      if (id == null) { // checkout currently doesn't exist - create one
        this.shopifyService.client.checkout.create().then(checkout => {
          this.cartItems = checkout.lineItems;
          this.totalPrice = checkout.totalPrice;
          this.currencyCode = checkout.lineItemsSubtotalPrice.currencyCode;
          this.webUrl = checkout.webUrl;
          this.orderStatus = checkout.completedAt;

          this.checkoutId = checkout.id;
          this.storage.set("checkoutId", this.checkoutId);
        }).catch(err => {
          this.presentAlert(err);
        });
      } else { // checkout exists - populate the information from it
        this.checkoutId = id;
        this.shopifyService.client.checkout.fetch(this.checkoutId).then(checkout => {
          this.cartItems = checkout.lineItems;
          for (let item of this.cartItems) {
            this.itemIds.push(item.id);
          }
          this.webUrl = checkout.webUrl;
          this.totalPrice = checkout.totalPrice;
          this.currencyCode = checkout.lineItemsSubtotalPrice.currencyCode;
          this.orderStatus = checkout.completedAt;
          
          if (this.orderStatus != null) { //if checkout is completed, create a new one
            this.shopifyService.client.checkout.create().then(checkout => {
              this.cartItems = checkout.lineItems;
              this.totalPrice = checkout.totalPrice;
              this.currencyCode = checkout.lineItemsSubtotalPrice.currencyCode;
              this.webUrl = checkout.webUrl;
              this.orderStatus = checkout.completedAt;

              this.checkoutId = checkout.id;
              this.storage.set("checkoutId", this.checkoutId);
              
              this.ionViewWillEnter();
            }).catch(err => {
              this.presentAlert(err);
            });
          }
        }).catch(err => {
          this.presentAlert(err);
        });
      }
    });
  }

  // increase quantity of item in cart
  quantityInc(item) {
    let newQuantity = item.quantity + 1;
    this.storage.get("checkoutId").then(id => {
      const lineItemsToUpdate = [{ id: item.id, quantity: newQuantity }];
      this.shopifyService.client.checkout.updateLineItems(id, lineItemsToUpdate).then(checkout => {
        console.log(checkout.lineItems);
        this.ionViewWillEnter();
      });
    });
  }

  // decrease quantity of item in cart
  quantityDec(item) {
    if (item.quantity == 1) {
      this.removeItem(item);
    } else {
      let newQuantity = item.quantity - 1;
      this.storage.get("checkoutId").then(id => {
        const lineItemsToUpdate = [{ id: item.id, quantity: newQuantity }];
        this.shopifyService.client.checkout.updateLineItems(id, lineItemsToUpdate).then(checkout => {
          console.log(checkout.lineItems);
          this.ionViewWillEnter();
        });
      });
    }
  }

  // remove item from cart
  removeItem(item){
    const lineItemIdsToRemove = [ item.id ];
    this.shopifyService.client.checkout.removeLineItems(this.checkoutId, lineItemIdsToRemove).then(checkout => {
      console.log(checkout);
      this.itemIds = this.itemIds.filter(id => {
        return id != item.id;
      })
      this.ionViewWillEnter();
    });
  }

  // remove ALL items from cart
  clearCart() {
    this.shopifyService.client.checkout.removeLineItems(this.checkoutId, this.itemIds).then(checkout => {
      console.log(checkout);
      this.itemIds = [];
      this.ionViewWillEnter();
    });
  }
  
  // navigate to browser to complete checkout
  checkout() {
    this.iab.create(this.webUrl);
    this.navToTracker();
  }

  navToTracker() {
    this.navCtrl.navigateForward("/tabs/track-progress");
  }

  navToPopover() {
    this.navCtrl.navigateForward("popover");
  }

  async presentAlert(err) {
    const alert = await this.alertController.create({
      header: err,
      message: err.message,
      buttons: ["OK"]
    });
    await alert.present();
  }

  change() {
    console.log("myBoolean", this.myBoolean);
    if(this.myBoolean == true){
      this.isChecked = false;
    }
    else if(this.myBoolean == false){
      this.isChecked == true;
    }
    console.log("isChecked", this.isChecked);
  }
}
