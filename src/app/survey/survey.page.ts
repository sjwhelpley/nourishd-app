import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular'
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../services/shopify.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.page.html',
  styleUrls: ['./survey.page.scss'],
})
export class SurveyPage implements OnInit {

  public levels: any;
  public types: any;
  public mainflows: any;
  public surveyType: any;
  public numDays: any;
  public frequency: any;
  public juice: any = [];
  public id: string;
  public cleanse: any;
  public usableJuices: any;
  public head: any;

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private loadingController: LoadingController,
    private shopifyService: ShopifyService,
  ) {
    const allJuiceID = "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzEzNzMzOTI0MDU0Ng==";

    this.shopifyService.client.collection.fetchWithProducts(allJuiceID).then((collection) => {
      this.usableJuices = collection.products;
      this.usableJuices.unshift(this.head);
    });
    
    this.storage.get('surveytype').then((type) => {
      this.surveyType = type;

      if (this.surveyType == "juice") {
        this.levels = [
          {
            name: "Easy",
            iconName: "bonfire",
            class: "border",
          },
          {
            name: "Medium",
            iconName: "sunny",
            class: "border",
          },
          {
            name: "Hard",
            iconName: "flash",
            class: "border",
          },
        ];

        this.types = [
          {
            name: "Broths and Soups",
            iconName: "star-outline",
            class: "border",
          },
          {
            name: "Half and Half",
            iconName: "star-half",
            class: "border",
          },
          {
            name: "All Juices",
            iconName: "star",
            class: "border",
          },
        ];
      } else {
        this.levels = [
          {
            name: "Easy (pasta)",
            iconName: "bonfire",
            class: "border",
          },
          {
            name: "Medium",
            iconName: "sunny",
            class: "border",
          },
          {
            name: "Hard (no carbs)",
            iconName: "flash",
            class: "border",
          },
        ]
        
        this.types = [
          {
            name: "Majority Warm",
            iconName: "flame",
            class: "border",
          },
          {
            name: "Majority Cold",
            iconName: "snow",
            class: "border",
          }
        ];
      }

      this.mainflows = [
        {
          name: "Gut Health",
          iconName: "sync",
          class: "border",
        },
        {
          name: "Detoxification",
          iconName: "sunny",
          class: "border",
        },
        {
          name: "Weight Loss",
          iconName: "body",
          class: "border",
        },
      ];

      this.frequency = [
        {
          name: "Working Mom",
          desc: "Delivery every five days for someone on the go",
          icon: "briefcase",
          class: "border",
        },
        {
          name: "Part-Time Creative",
          desc: "Delivery every three days for those who...",
          icon: "brush",
          class: "border",
        },
        {
          name: "Beach Bum",
          desc: "Delivery every two days for...",
          icon: "glasses",
          class: "border",
        }
      ];
    });

    this.numDays = [
      {
        days: "ONE",
        class: "border",
      },
      {
        days: "THREE",
        class: "border",
      },
      {
        days: "FIVE",
        class: "border",
      },
      {
        days: "SEVEN",
        class: "border",
      },
      {
        days: "TEN",
        class: "border",
      },
    ];
  }

  ngOnInit() {
  }

  name(n) {
    console.log(n);
  }

  ionViewDidEnter() {
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  itemClicked(icon, icons) {
    icons.forEach(entry => {
      if (entry == icon) {
        icon.class = "border-active";
      } else {
        entry.class = "border";
      }
    });
  }

  submit(){
    if(this.getSelectedSurveyInfo(this.types) == "Broths and Soups"){
      this.presentAlert("Your recommended cleanse is the Vegan Broth Cleanse! Go to the store to buy it now!!");
    }
    else if (this.getSelectedSurveyInfo(this.types) == "Half and Half") {
      this.presentAlert("Your recommended cleanse is the Green Warrior Cleanse! Go to the store to buy it now!!");
    }
    else if (this.getSelectedSurveyInfo(this.types) == "All Juices") {
      if (this.getSelectedSurveyInfo(this.mainflows) == "Gut Health") {
        this.presentAlert("Your recommended cleanse is the MindBodySoul Cleanse! Go to the store to buy it now!!");
      }
      else if (this.getSelectedSurveyInfo(this.mainflows) == "Detoxification") {
        this.presentAlert("Your recommended cleanse is the Celery Cleanse! Go to the store to buy it now!!");
      }
      else if (this.getSelectedSurveyInfo(this.mainflows) == "Weight Loss") {
        this.presentAlert("Your recommended cleanse is the Celery Cleanse! Go to the store to buy it now!!");
      }
    } 
  }

  getSelectedSurveyInfo(survey): String {
    let name = "";
    survey.forEach(item =>{
      if(item.class == "border-active") {
        name = item.name;
      }
    })
    return(name);
  }

  navToStore(){
    this.navCtrl.navigateForward('tabs/store');
  }

  async presentAlert(details) {
    const alert = await this.alertCtrl.create({
      header: "Alert",
      message: details,
      buttons: [{text: "OK"}, {text: "Go To Store", handler: () => {
        this.navCtrl.navigateForward("nourishd-cleanses")
      }}] 
    });
    await alert.present();
  }
}
