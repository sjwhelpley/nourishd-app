import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllJuicesPage } from './all-juices.page';

describe('AllJuicesPage', () => {
  let component: AllJuicesPage;
  let fixture: ComponentFixture<AllJuicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllJuicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllJuicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
