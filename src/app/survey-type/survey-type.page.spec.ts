import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyTypePage } from './survey-type.page';

describe('SurveyTypePage', () => {
  let component: SurveyTypePage;
  let fixture: ComponentFixture<SurveyTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyTypePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
