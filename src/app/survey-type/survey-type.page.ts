import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-survey-type',
  templateUrl: './survey-type.page.html',
  styleUrls: ['./survey-type.page.scss'],
})
export class SurveyTypePage implements OnInit {

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  async presentComingSoon(){
    const alert = await this.alertController.create({
      header: "Meal Plans",
      message: "Coming Soon",
      buttons: ["OK"]
    });

    await alert.present();
  }

  navToSurvey(type){
    if(type){
      this.storage.set('surveytype', "juice");
    } else {
      this.storage.set('surveytype', "meal");
    }
    this.navCtrl.navigateForward('survey');
  }
}
