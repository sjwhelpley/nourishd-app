import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage{

  constructor(
    private navCtrl: NavController,
  ) {
  
   }

   navToCart(){
     this.navCtrl.navigateBack("tabs/cart");
   }

}
