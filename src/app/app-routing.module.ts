import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'all-juices', loadChildren: './all-juices/all-juices.module#AllJuicesPageModule' },
  { path: 'survey', loadChildren: './survey/survey.module#SurveyPageModule' },
  { path: 'nourishd-cleanses', loadChildren: './existing-programs/nourishd-cleanses/nourishd-cleanses.module#NourishdCleansesPageModule' },
  { path: 'nourishd-meal-plans', loadChildren: './existing-programs/nourishd-meal-plans/nourishd-meal-plans.module#NourishdMealPlansPageModule' },
  { path: 'choose-boosters', loadChildren: './existing-programs/choose-boosters/choose-boosters.module#ChooseBoostersPageModule' },
  { path: 'choose-cleanse', loadChildren: './existing-programs/choose-cleanse/choose-cleanse.module#ChooseCleansePageModule' },
  { path: 'create-plan', loadChildren: './customized-programs/create-plan/create-plan.module#CreatePlanPageModule' },
  { path: 'program-details', loadChildren: './existing-programs/program-details/program-details.module#ProgramDetailsPageModule' },
  { path: 'custom-program-details', loadChildren: './customized-programs/program-details/program-details.module#ProgramDetailsPageModule' },
  { path: 'survey-type', loadChildren: './survey-type/survey-type.module#SurveyTypePageModule' },
  { path: 'track-progress', loadChildren: './track-progress/track-progress.module#TrackProgressPageModule' },
  { path: 'popover', loadChildren: './popover/popover.module#PopoverPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }


