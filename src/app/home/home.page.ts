import { Component } from '@angular/core';
import {NavController, AlertController} from '@ionic/angular';
import { setCheckNoChangesMode } from '@angular/core/src/render3/state';
import { LoadingController } from '@ionic/angular'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  email: string;
  password: string
  public users: any;

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private loadingController: LoadingController
  ) { }

  ngOnInit() { }

  skip() {
    this.navCtrl.navigateForward('tabs');
  }

  async presentAlert(err) {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Failed to login',
      message: err,
      buttons: ['OK']
    });
  
    await alert.present();
  }

  ionViewDidEnter(){
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
