import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';

@Component({
  selector: "app-explore",
  templateUrl: "./explore.page.html",
  styleUrls: ["./explore.page.scss"]
})
export class ExplorePage {

  constructor(
    private loadingController: LoadingController,
    private navCtrl: NavController
  ) { }

  ionViewDidEnter(){
    this.presentLoading();
  }

  navJuices() {
    this.navCtrl.navigateForward('all-juices');
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    // console.log('Loading dismissed!');
  }
}