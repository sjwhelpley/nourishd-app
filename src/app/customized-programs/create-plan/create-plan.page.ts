import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular'
import { FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../../services/shopify.service';

@Component({
  selector: 'app-create-plan',
  templateUrl: './create-plan.page.html',
  styleUrls: ['./create-plan.page.scss'],
})
export class CreatePlanPage {
  myform: FormGroup; // form for HTML

  public cleanseId: any; // id of the chosen cleanse product
  public usableJuices: any; // list of juices available
  public entries: any; // times & choosen juices for the current day
  public attributeObject: any = []; // array of formated custom attributes
  public dayNum: number; // number of days in cleanse
  public counter: number = 1; // current day choosing juices for

  constructor (
    private storage: Storage,
    private navCtrl: NavController,
    private shopifyService: ShopifyService,
    private loadingController: LoadingController
  ) {
    this.storage.get('numDays').then((num) => {
      // Get the number of days of chosen cleanse
      this.dayNum = num;
    });

    // To be added to front of array to provide instructions
    let head = {
      title: "Select a Juice"
    }

    // Find correct variant id based on chosen num of days of cleanse
    let customCleanseId = "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzEzNzU5NTE1ODYyNg==";
    this.shopifyService.client.collection.fetchWithProducts(customCleanseId).then((collection) => {
      let variants = collection.products[0].variants;
      for(let i = 0; i < variants.length; i++) {
        if(String(this.dayNum) == variants[i].title) {
          this.cleanseId = variants[i].id;
        }
      }
    });

    // Create array with all of the available juices 
    const allJuiceID = "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzEzNzMzOTI0MDU0Ng==";
    this.shopifyService.client.collection.fetchWithProducts(allJuiceID).then((collection) => {
      this.usableJuices = collection.products;
      this.usableJuices.unshift(head);
    });

    // Array to store user's entries for each day
    this.entries = [
      {
        time: "8am",
        cleanse: "",
      },
      {
        time: "10am",
        cleanse: "",
      },
      {
        time: "12pm",
        cleanse: "",
      },
      {
        time: "3pm",
        cleanse: "",
      },
      {
        time: "5pm",
        cleanse: "",
      },
      {
        time: "7pm",
        cleanse: "",
      }
    ];
  }

  // Called when user clicks button to continue
  submitDay() {
    if (this.counter < this.dayNum) {
      // Format all times & juices
      let schedule = "";
      for(let entry of this.entries) {
        schedule += `${entry.time}: ${entry.cleanse} `;
      }
      // Add key & value pair for each day to entryObject
      this.attributeObject.push({
        key: `Day ${this.counter}`, 
        value: schedule
      });

      // Increment counter keeping track of current day being filled out
      this.counter++;
    } else if (this.counter == this.dayNum) { // Done choosing juices - add to checkout
      // Format all times and juices
      let schedule = "";
      for(let entry of this.entries) {
        schedule += `${entry.time}: ${entry.cleanse} `;
      }
      // Add key & value pair for each day to entryObject
      this.attributeObject.push({
        key: `Day ${this.counter}`, 
        value: schedule
      });

      this.storage.get('checkoutId').then((id) => {
        // Create array with the custom cleanse - including the attributes listing the juices
        let lineItemsToAdd = [];
        let itemAdd = {
          variantId: this.cleanseId,
          quantity: 1,
          customAttributes: this.attributeObject
        };
        lineItemsToAdd.push(itemAdd);
        this.shopifyService.client.checkout.addLineItems(id, lineItemsToAdd).then((checkout) => {
          console.log(checkout);
        });
      });
      this.navToCart();
    }
  }

  navToCart() {
    this.navCtrl.navigateForward('tabs/store');
  }

  ionViewDidEnter() {
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    // console.log('Loading dismissed!');
  }
}
