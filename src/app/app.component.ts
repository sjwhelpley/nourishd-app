import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import * as shuffleArray from 'shuffle-array'
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  ecouragingMessages= [
    'Did you enjoy your cleanse, come back and try another.',
    'We have not heard back from you, are you ready for another cleanse.',
    'Are you feeling unhealthy, try one of our cleanses.',
    'Check your Nourishd points to see if you have a discount.',
    'How are you? Are you ready for another cleanse?',
    'Please leave a rate.',
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private localNotification: LocalNotifications
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      shuffleArray(this.ecouragingMessages).forEach((message, index) => {
        this.localNotification.schedule({
          id: index,
          text: message,
          trigger:{
            in: 1 + (index * 2),
            unit: ELocalNotificationTriggerUnit.WEEK,
          },
        })
      });
    });
  }
}
