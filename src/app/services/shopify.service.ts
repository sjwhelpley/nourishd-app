import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import Client from "shopify-buy/index.unoptimized.umd";

@Injectable({
  providedIn: 'root'
})
export class ShopifyService {

  public client = Client.buildClient({
    domain: "nourishdza.myshopify.com",
    storefrontAccessToken: "f3205c1609da880652941f66aeb89b9d"
  });

  public storefrontAccessToken = "f3205c1609da880652941f66aeb89b9d";

  // Adding items to cart
  public variantId: number;
  public numDays: number;
  public startDate: string;

  constructor(
    private storage: Storage
  ) { 
    this.storage.get("checkoutId").then((id) => {
      if(id == null) {
        this.client.checkout.create().then((checkout) => {
          this.storage.set("checkoutId", id);
        }).catch((err) => {
          console.log(err);
        });
      } 
    });
  }
}
