import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { NavController, AlertController } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';

import { ShopifyService } from "../services/shopify.service";

@Component({
  selector: 'app-store',
  templateUrl: './store.page.html',
  styleUrls: ['./store.page.scss'],
})
export class StorePage implements OnInit {

  constructor(
    private navCtrl: NavController,
    private storage : Storage,
    private shopifyService: ShopifyService,
    public alertController: AlertController,
    private loadingController: LoadingController
  ) { }

  public checkoutId: any;
  public quantity: number = 1;
  public items: any[] = [];

  async presentAlert(err) {
    const alert = await this.alertController.create({
      header: err,
      message: err.message,
      buttons: ["OK"]
    });

    await alert.present();
  }

  async presentComingSoon(){
    const alert = await this.alertController.create({
      header: "Meal Plans",
      message: "Coming Soon",
      buttons: ["OK"]
    });

    await alert.present();
  }

  ngOnInit() {
    this.storage.get('checkoutId').then((val)=>{
      this.checkoutId = val;  
      if (this.checkoutId == null) {
        this.shopifyService.client.checkout
          .create()
          .then(checkout => {
            this.items = checkout.LineItems;
            this.checkoutId = checkout.id;
            this.storage.set("checkoutId", this.checkoutId);
          })
          .catch(err => {
            debugger;
            this.presentAlert(err);
          });
        }
    });
  }
  
  navToAllJuices(){
    this.navCtrl.navigateForward('all-juices');
  }

  navToDetails() {
    this.navCtrl.navigateForward('custom-program-details');
  }
  
  navToSurvey() {
    this.navCtrl.navigateForward('survey-type');
  }

  navToCleanses() {
    this.navCtrl.navigateForward('nourishd-cleanses');
  }

  navToMealPlans() {
    this.navCtrl.navigateForward('nourishd-meal-plans');
  }

  ionViewDidEnter() {
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
