import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseCleansePage } from './choose-cleanse.page';

describe('ChooseCleansePage', () => {
  let component: ChooseCleansePage;
  let fixture: ComponentFixture<ChooseCleansePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseCleansePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseCleansePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
