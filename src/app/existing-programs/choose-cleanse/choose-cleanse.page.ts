import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular'
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../../services/shopify.service';

@Component({
  selector: 'app-choose-cleanse',
  templateUrl: './choose-cleanse.page.html',
  styleUrls: ['./choose-cleanse.page.scss'],
})
export class ChooseCleansePage implements OnInit {
  public cleanse: any;
  public id: string;

  constructor(
    private navCtrl: NavController,
    private shopifyService: ShopifyService,
    private storage: Storage,
    private loadingController: LoadingController,
  ) {
    this.id = localStorage.getItem("id");

    this.storage.get('id').then((val) => {
      const productId = val;
      this.shopifyService.client.product.fetch(productId).then((product: any) => {
        this.cleanse = product;
      });
    });
  }

  ngOnInit() {
  }

  navDetails() {
    this.storage.set('itemId', this.cleanse.id);
    this.navCtrl.navigateForward('program-details');
  }

  ionViewDidEnter(){
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
