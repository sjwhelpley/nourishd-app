import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../../services/shopify.service';

@Component({
  selector: 'app-choose-boosters',
  templateUrl: './choose-boosters.page.html',
  styleUrls: ['./choose-boosters.page.scss'],
})
export class ChooseBoostersPage implements OnInit {
  public boosterInfo: string = "";

  public shortBoosters: any[] = [];
  public description: string = "";
  public descriptionHtml: string[] = [];

  public tallBoosters: any[] = [];
  public tallDescription: string = "";
  public tallDescriptionHtml: string[] = [];

  public brothBoosters: any[] = [];
  public brothDescription: string = "";
  public brothDescriptionHtml: string[] = [];

  public types: any;
  public juiceClicked: boolean = false;
  public shotClicked: boolean = false;
  public brothClicked: boolean = false;

  public buttonColor: string = 'white';

  public choosenBoosters: any[] = [];

  constructor(
    private navCtrl: NavController,
    private sanitizer: DomSanitizer,
    private shopifyService: ShopifyService,
    private loadingController: LoadingController,
    private storage: Storage
  ) { 
    // Querying Shot Boosters
    const query = this.shopifyService.client.graphQLClient.query((root) => {
      root.addConnection('products', {args: { first: 25, query: "product_type:Booster" }}, (Product) => {
        Product.add('title');
        Product.add('description');
        Product.add('descriptionHtml');
        Product.addConnection('variants', {args: {first: 10}}, (variants) => {
          variants.add('id');
          variants.add('title');
        });
        Product.addConnection('images', {args: {first: 10}}, (images) => {
          images.add('src');
        });
      });
    });

    this.shopifyService.client.graphQLClient.send(query).then(({ model, data }) => {
      this.shortBoosters = model.products;
      let rawHtml = this.shortBoosters[0].descriptionHtml;
      this.boosterInfo = rawHtml.substring(91,258);
      
      this.shortBoosters.forEach(booster => {
        this.descriptionHtml.push(booster.descriptionHtml.substring(259));
      });
    });

    // Querying Juice Boosters
    const tallQuery = this.shopifyService.client.graphQLClient.query((root) => {
      root.addConnection('products', {args: { first: 25, query: "product_type:TallBooster" }}, (Product) => {
        Product.add('title');
        Product.add('description');
        Product.add('descriptionHtml');
        Product.addConnection('variants', {args: {first: 10}}, (variants) => {
          variants.add('id');
          variants.add('title');
        });
        Product.addConnection('images', {args: {first: 10}}, (images) => {
          images.add('src');
        });
      });
    });

    this.shopifyService.client.graphQLClient.send(tallQuery).then(({ model, data }) => {
      this.tallBoosters = model.products;
      
      this.tallBoosters.forEach(booster => {
        this.tallDescriptionHtml.push(booster.descriptionHtml.substring(259));
      });
    });

    // Querying Broth Boosters
    const brothQuery = this.shopifyService.client.graphQLClient.query((root) => {
      root.addConnection('products', {args: { first: 25, query: "product_type:BrothBooster" }}, (Product) => {
        Product.add('title');
        Product.add('description');
        Product.add('descriptionHtml');
        Product.addConnection('variants', {args: {first: 10}}, (variants) => {
          variants.add('id');
          variants.add('title');
        });
        Product.addConnection('images', {args: {first: 10}}, (images) => {
          images.add('src');
        });
      });
    });

    this.shopifyService.client.graphQLClient.send(brothQuery).then(({ model, data }) => {
      this.brothBoosters = model.products;

      this.brothBoosters.forEach(booster => {
        this.brothDescriptionHtml.push(booster.descriptionHtml.substring(259));
      });
    });

    this.types = [
      {
        name: "Juices",
        iconName: "sunny",
        class: "border",
      },
      {
        name: "Shots",
        iconName: "flash",
        class: "border",
      },
      {
        name: "Broths",
        iconName: "cafe",
        class: "border",
      },
    ];
  }

  ngOnInit() {
  }

  pickBooster(id) {
    if(document.getElementById(id).style.outline == "pink solid thick") {
      document.getElementById(id).style.outline = "thick solid white";
      this.choosenBoosters = this.choosenBoosters.filter((boosterId) => {
        return boosterId != id;
      });
    } else {
      document.getElementById(id).style.outline = "pink solid thick";
      this.choosenBoosters.push(id);
    }
  }

  navCart(){
    this.navCtrl.navigateForward('tabs/cart');
  }

  async addBoosters() {
    this.storage.get('numOfDays').then((num) => {
      this.storage.get('checkoutId').then(async (id) => {
        for(let booster of this.choosenBoosters) {
          await this.shopifyService.client.product.fetch(booster).then((product) => {
            switch(num) {
              case 1:
                this.shopifyService.variantId = product.variants[0].id;
                break;
              case 3:
                  this.shopifyService.variantId = product.variants[1].id;
                break;
              case 5:
                  this.shopifyService.variantId = product.variants[2].id;
                break;
              case 7:
                  this.shopifyService.variantId = product.variants[3].id;
                break;
              case 10:
                  this.shopifyService.variantId = product.variants[4].id;
                break;
              default:
            }
            const lineItemsToAdd = [
              {
                variantId: this.shopifyService.variantId,
                quantity: 1
              }
            ];
            this.shopifyService.client.checkout.addLineItems(id, lineItemsToAdd).then((checkout) => {
              console.log(checkout.lineItems);
            });
          });
       }
      });
      this.navCtrl.navigateForward('tabs/cart');
    });
  }

  ionViewDidEnter(){
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  itemClicked(icon, icons) {
    if(icon.name == "Juices"){
      this.juiceClicked = true;
      this.brothClicked = false;
      this.shotClicked = false;
    } else if (icon.name == "Broths") {
      this.juiceClicked = false;
      this.brothClicked = true;
      this.shotClicked = false;
    } else if (icon.name == "Shots") {
      this.juiceClicked = false;
      this.brothClicked = false;
      this.shotClicked = true;
    }

    icons.forEach(entry => {
      if(entry == icon){
        icon.class = "border-active";
      } else {
        entry.class = "border";
      }
    });
  }
}
