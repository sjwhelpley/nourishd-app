import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseBoostersPage } from './choose-boosters.page';

describe('ChooseBoostersPage', () => {
  let component: ChooseBoostersPage;
  let fixture: ComponentFixture<ChooseBoostersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseBoostersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseBoostersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
