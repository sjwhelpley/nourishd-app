import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChooseBoostersPage } from './choose-boosters.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseBoostersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChooseBoostersPage]
})
export class ChooseBoostersPageModule {}
