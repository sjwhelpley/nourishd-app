import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../../services/shopify.service';

@Component({
  selector: 'app-program-details',
  templateUrl: './program-details.page.html',
  styleUrls: ['./program-details.page.scss'],
})
export class ProgramDetailsPage implements OnInit {
  minDate;
  startDate;
  public days: any;

  constructor(
    private shopifyService: ShopifyService,
    private navCtrl: NavController,
    private storage: Storage
  ) { 
    this.days = [
      {
        day: "ONE",
        dayNum: 1,
        class: "border",
      },
      {
        day: "THREE",
        dayNum: 3,
        class: "border",
      },
      {
        day: "FIVE",
        dayNum: 5,
        class: "border",
      },
      {
        day: "SEVEN",
        dayNum: 7,
        class: "border",
      },
      {
        day: "TEN",
        dayNum: 10,
        class: "border",
      },
    ]   
  }

  ngOnInit() {
    let date = new Date();
    date.setDate(date.getDate() + 2);
    this.minDate = this.formatDate(date);
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  setNumDays(num) {
    this.shopifyService.numDays = parseInt(num);
    this.storage.set('numOfDays', parseInt(num));
  }

  navBoosters() {
    this.shopifyService.startDate = this.startDate;
    this.storage.get('itemId').then((id) => {
      this.shopifyService.client.product.fetch(id).then((product) => {
        this.storage.get('numOfDays').then((num)=> {
          switch(num) {
            case 1:
              this.shopifyService.variantId = product.variants[0].id;
              break;
            case 3:
                this.shopifyService.variantId = product.variants[1].id;
              break;
            case 5:
                this.shopifyService.variantId = product.variants[2].id;
              break;
            case 7:
                this.shopifyService.variantId = product.variants[3].id;
              break;
            case 10:
                this.shopifyService.variantId = product.variants[4].id;
              break;
            default:
          }

          const lineItemsToAdd = [
            {
              variantId: this.shopifyService.variantId,
              quantity: 1,
              customAttributes: [{key: "startDate", value: this.startDate}]
            }
          ];

          this.storage.get("checkoutId").then((id) => {
            this.shopifyService.client.checkout.addLineItems(id, lineItemsToAdd).then((checkout) => {
                this.navCtrl.navigateForward("choose-boosters");
              });
          });
        });
      });
    });
  }

  itemClicked(icon, icons){
    icons.forEach(entry => {
      if(entry == icon){
        icon.class = "border-active";
      } else {
        entry.class = "border";
      }
    });
    this.setNumDays(icon.dayNum);
  }
}
