import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NourishdMealPlansPage } from './nourishd-meal-plans.page';

const routes: Routes = [
  {
    path: '',
    component: NourishdMealPlansPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NourishdMealPlansPage]
})
export class NourishdMealPlansPageModule {}
