import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NourishdCleansesPage } from './nourishd-cleanses.page';

describe('NourishdCleansesPage', () => {
  let component: NourishdCleansesPage;
  let fixture: ComponentFixture<NourishdCleansesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NourishdCleansesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NourishdCleansesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
