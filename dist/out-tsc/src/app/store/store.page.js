import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var StorePage = /** @class */ (function () {
    function StorePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    StorePage.prototype.ngOnInit = function () {
    };
    StorePage.prototype.navToAllJuices = function () {
        this.navCtrl.navigateForward('all-juices');
    };
    StorePage.prototype.navToSurvey = function () {
        this.navCtrl.navigateForward('survey');
    };
    StorePage.prototype.navToCleanses = function () {
        this.navCtrl.navigateForward('nourishd-cleanses');
    };
    StorePage = tslib_1.__decorate([
        Component({
            selector: 'app-store',
            templateUrl: './store.page.html',
            styleUrls: ['./store.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], StorePage);
    return StorePage;
}());
export { StorePage };
//# sourceMappingURL=store.page.js.map