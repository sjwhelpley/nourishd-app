import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var ExplorePage = /** @class */ (function () {
    function ExplorePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.events = [
            {
                name: "Fun Run",
                desc: "",
                price: "From 300R",
                img: "../../assets/practice-events/run.jpg",
            },
            {
                name: "Workshop",
                desc: "",
                price: "From 400R",
                img: "../../assets/practice-events/workshop.jpg",
            },
        ];
    }
    ExplorePage.prototype.ngOnInit = function () {
    };
    ExplorePage.prototype.addToCart = function (item) {
        console.log(item + " added to cart!");
    };
    ExplorePage = tslib_1.__decorate([
        Component({
            selector: 'app-explore',
            templateUrl: './explore.page.html',
            styleUrls: ['./explore.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], ExplorePage);
    return ExplorePage;
}());
export { ExplorePage };
//# sourceMappingURL=explore.page.js.map