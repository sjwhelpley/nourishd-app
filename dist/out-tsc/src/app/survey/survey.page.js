import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var SurveyPage = /** @class */ (function () {
    function SurveyPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.goals = [
            {
                ionIconName: "infinite",
                goal: "LONGEVITY"
            },
            {
                ionIconName: "happy",
                goal: "IMPROVED MOOD"
            },
            {
                ionIconName: "flash",
                goal: "ENERGY"
            },
            {
                ionIconName: "star",
                goal: "IMMUNITY"
            },
            {
                ionIconName: "moon",
                goal: "IMPROVED SLEEP"
            },
            {
                ionIconName: "bulb",
                goal: "BRAIN"
            },
        ];
    }
    SurveyPage.prototype.ngOnInit = function () {
    };
    SurveyPage.prototype.name = function (n) {
        console.log(n);
    };
    SurveyPage = tslib_1.__decorate([
        Component({
            selector: 'app-survey',
            templateUrl: './survey.page.html',
            styleUrls: ['./survey.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], SurveyPage);
    return SurveyPage;
}());
export { SurveyPage };
//# sourceMappingURL=survey.page.js.map