import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SurveyPage } from './survey.page';
var routes = [
    {
        path: '',
        component: SurveyPage
    }
];
var SurveyPageModule = /** @class */ (function () {
    function SurveyPageModule() {
    }
    SurveyPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SurveyPage]
        })
    ], SurveyPageModule);
    return SurveyPageModule;
}());
export { SurveyPageModule };
//# sourceMappingURL=survey.module.js.map