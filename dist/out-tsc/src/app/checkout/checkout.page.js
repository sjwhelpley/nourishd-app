import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var CheckoutPage = /** @class */ (function () {
    function CheckoutPage() {
    }
    CheckoutPage.prototype.ngOnInit = function () {
    };
    CheckoutPage = tslib_1.__decorate([
        Component({
            selector: 'app-checkout',
            templateUrl: './checkout.page.html',
            styleUrls: ['./checkout.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], CheckoutPage);
    return CheckoutPage;
}());
export { CheckoutPage };
//# sourceMappingURL=checkout.page.js.map