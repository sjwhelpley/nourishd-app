import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var AllJuicesPage = /** @class */ (function () {
    function AllJuicesPage() {
        this.juices = [
            {
                name: "Happy Hippy",
                desc: "Kale, spinach, celery, cucumber, lemon, ginger"
            },
            {
                name: "Green Goddess",
                desc: "Apple, cucumber, ginger, mint"
            },
            {
                name: "Pineapple Dreams",
                desc: "Pineapple, cucumber, apple, lemon"
            },
            {
                name: "Feelin' Da Beet of my Heart",
                desc: "Beetroot, apple, cucumber, carrot, ginger, mint, lemon"
            },
            {
                name: "Glow and Flow",
                desc: "Orange, apple, ginger, basil, chilli, cayenne pepper"
            },
            {
                name: "Black Buchu",
                desc: "Activated charcoal, buchu leaves, cucumber, buchu water, apple, ginger, lime"
            },
            {
                name: "Fruuti Tuuti",
                desc: "Pineapple, grapefruit, lemon"
            },
            {
                name: "Gaian Green",
                desc: "Celery, coriander, lemon, cucumber"
            },
            {
                name: "Sunrise Sun Salutaion",
                desc: "Orange, carrot, lemon, echinacea"
            },
            {
                name: "Rose Glow",
                desc: "*NEW* Hydrating coconut water infused with rose, chia seeds and lemon"
            },
            {
                name: "Honey Love",
                desc: "Grapefruit, ornage, lemon, bee pollen, raw honey,tumeric, black pepper"
            }
        ];
    }
    AllJuicesPage.prototype.ngOnInit = function () {
    };
    AllJuicesPage.prototype.addToCart = function (item) {
        console.log(item + " added to cart!");
    };
    AllJuicesPage = tslib_1.__decorate([
        Component({
            selector: 'app-all-juices',
            templateUrl: './all-juices.page.html',
            styleUrls: ['./all-juices.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], AllJuicesPage);
    return AllJuicesPage;
}());
export { AllJuicesPage };
//# sourceMappingURL=all-juices.page.js.map