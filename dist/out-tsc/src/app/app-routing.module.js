import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: './home/home.module#HomePageModule' },
    { path: 'registration', loadChildren: './registration/registration.module#RegistrationPageModule' },
    { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
    { path: 'all-juices', loadChildren: './all-juices/all-juices.module#AllJuicesPageModule' },
    { path: 'survey', loadChildren: './survey/survey.module#SurveyPageModule' },
    { path: 'nourishd-cleanses', loadChildren: './nourishd-cleanses/nourishd-cleanses.module#NourishdCleansesPageModule' },
    { path: 'checkout', loadChildren: './checkout/checkout.module#CheckoutPageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map