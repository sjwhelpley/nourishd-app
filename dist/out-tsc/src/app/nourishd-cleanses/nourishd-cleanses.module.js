import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NourishdCleansesPage } from './nourishd-cleanses.page';
var routes = [
    {
        path: '',
        component: NourishdCleansesPage
    }
];
var NourishdCleansesPageModule = /** @class */ (function () {
    function NourishdCleansesPageModule() {
    }
    NourishdCleansesPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [NourishdCleansesPage]
        })
    ], NourishdCleansesPageModule);
    return NourishdCleansesPageModule;
}());
export { NourishdCleansesPageModule };
//# sourceMappingURL=nourishd-cleanses.module.js.map