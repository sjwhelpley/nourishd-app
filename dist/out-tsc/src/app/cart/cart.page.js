import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var CartPage = /** @class */ (function () {
    function CartPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.qty1 = 1;
        this.qty2 = 1;
    }
    CartPage.prototype.ngOnInit = function () {
    };
    CartPage.prototype.increment1 = function () {
        this.qty1 += 1;
    };
    CartPage.prototype.decrement1 = function () {
        if (this.qty1 - 1 < 1) {
            this.qty1 = 1;
        }
        else {
            this.qty1 -= 1;
        }
    };
    CartPage.prototype.increment2 = function () {
        this.qty2 += 1;
    };
    CartPage.prototype.decrement2 = function () {
        if (this.qty2 - 1 < 1) {
            this.qty2 = 1;
        }
        else {
            this.qty2 -= 1;
        }
    };
    CartPage.prototype.clearAll = function () {
    };
    CartPage.prototype.checkout = function () {
        this.navCtrl.navigateForward('checkout');
    };
    CartPage.prototype.delete = function () {
        this.navCtrl.navigateForward('tabs/explore');
    };
    CartPage = tslib_1.__decorate([
        Component({
            selector: 'app-cart',
            templateUrl: './cart.page.html',
            styleUrls: ['./cart.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], CartPage);
    return CartPage;
}());
export { CartPage };
//# sourceMappingURL=cart.page.js.map